import * as React from "react";
import { StyleSheet } from "react-native";

import { Statusbar, Text, View } from "../components/Themed";

export default function ModalScreen() {
  return (
    <View style={styles.container}>
      <Statusbar />
      <Text style={styles.title}>丑团</Text>
      <Text
        colorName="background"
        themeType="backgroundColor"
        themeName="primary"
        style={styles.date}
      >
        2021年10月26日 起
      </Text>
      <View
        style={styles.separator}
        lightColor="#eee"
        darkColor="rgba(255,255,255,0.1)"
      />
      <Text style={{ textAlign: "center" }}>
        某团给的满十二减十二优惠卷两张被收回 有感而发
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: 30,
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
  },
  separator: {
    marginVertical: 10,
    height: 1,
    width: "100%",
  },
  date: {
    borderRadius: 3,
    paddingVertical: 2,
    paddingHorizontal: 5,
    fontSize: 12,
    letterSpacing: 1,
  },
});
