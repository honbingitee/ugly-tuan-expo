import { Dimensions, Platform } from "react-native";

const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;
export const IOS = Platform.OS === "ios";

export default {
  window: {
    width,
    height,
  },
  isSmallDevice: width < 375,
  IOS,
};
