const primaryColorLight = "#51f";
const primaryColorDark = "#ccc";
const tintColorLight = "#2f95dc";
const tintColorDark = "#fff";

export default {
  light: {
    primary: primaryColorLight,
    text: "#000",
    background: "#fff",
    tint: tintColorLight,
    tabIconDefault: "#ccc",
    tabIconSelected: tintColorLight,
    deep: "#ccc",
  },
  dark: {
    primary: primaryColorDark,
    text: "#fff",
    background: "#000",
    tint: tintColorDark,
    tabIconDefault: "#ccc",
    tabIconSelected: tintColorDark,
    deep: "#666",
  },
};
